# build stage
FROM golang:alpine AS build-env
ADD . /src
RUN cd /src && CGO_ENABLED=0 GOOS=linux go build -a -ldflags="-s -w" -o goapp .

# final stage
FROM scratch
WORKDIR /app
COPY --from=build-env /src/goapp /app/goapp
ENTRYPOINT ["/app/goapp"]
