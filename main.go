package main

import (
	"net/http"
	"fmt"
	"time"
	"flag"
)

func loop(imgRequestPath, imgFeedPath string) {

	for ; ; {
		resp, err := http.Get(imgRequestPath)
		if err != nil {
			fmt.Println(err)
			continue
		}

		_, err = http.Post(imgFeedPath, "image/jpeg", resp.Body)

		if err != nil {
			fmt.Println(err)
		}

		time.Sleep(time.Millisecond * 25)
	}
}

func main() {

	requestPath := flag.String("request", "http://localhost:56000/jpeg", "Request path")
	feedPath := flag.String("feed", "http://localhost:8000/feed/", "Feed path")
	key := flag.String("key", "feednumberone", "Feed key")
	flag.Parse()

	loop(*requestPath, *feedPath + *key)
}
